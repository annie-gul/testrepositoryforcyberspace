// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PATCHERDEMO_PDDownloadableUI_generated_h
#error "PDDownloadableUI.generated.h already included, missing '#pragma once' in PDDownloadableUI.h"
#endif
#define PATCHERDEMO_PDDownloadableUI_generated_h

#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_SPARSE_DATA
#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_RPC_WRAPPERS
#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPDDownloadableUI(); \
	friend struct Z_Construct_UClass_UPDDownloadableUI_Statics; \
public: \
	DECLARE_CLASS(UPDDownloadableUI, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(UPDDownloadableUI)


#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPDDownloadableUI(); \
	friend struct Z_Construct_UClass_UPDDownloadableUI_Statics; \
public: \
	DECLARE_CLASS(UPDDownloadableUI, UUserWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(UPDDownloadableUI)


#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPDDownloadableUI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPDDownloadableUI) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPDDownloadableUI); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPDDownloadableUI); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPDDownloadableUI(UPDDownloadableUI&&); \
	NO_API UPDDownloadableUI(const UPDDownloadableUI&); \
public:


#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPDDownloadableUI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPDDownloadableUI(UPDDownloadableUI&&); \
	NO_API UPDDownloadableUI(const UPDDownloadableUI&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPDDownloadableUI); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPDDownloadableUI); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPDDownloadableUI)


#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_PRIVATE_PROPERTY_OFFSET
#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_12_PROLOG
#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_RPC_WRAPPERS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_INCLASS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_INCLASS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PATCHERDEMO_API UClass* StaticClass<class UPDDownloadableUI>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PathcerDemo_Source_PatcherDemo_PDDownloadableUI_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
