// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FPPatchStats;
#ifdef PATCHERDEMO_PDGameInstance_generated_h
#error "PDGameInstance.generated.h already included, missing '#pragma once' in PDGameInstance.h"
#endif
#define PATCHERDEMO_PDGameInstance_generated_h

#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPPatchStats_Statics; \
	PATCHERDEMO_API static class UScriptStruct* StaticStruct();


template<> PATCHERDEMO_API UScriptStruct* StaticStruct<struct FPPatchStats>();

#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_12_DELEGATE \
struct _Script_PatcherDemo_eventChunkMountedDelegate_Parms \
{ \
	int32 ChunkID; \
	bool Succeeded; \
}; \
static inline void FChunkMountedDelegate_DelegateWrapper(const FMulticastScriptDelegate& ChunkMountedDelegate, int32 ChunkID, bool Succeeded) \
{ \
	_Script_PatcherDemo_eventChunkMountedDelegate_Parms Parms; \
	Parms.ChunkID=ChunkID; \
	Parms.Succeeded=Succeeded ? true : false; \
	ChunkMountedDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_11_DELEGATE \
struct _Script_PatcherDemo_eventPatchCompleteDelegate_Parms \
{ \
	bool Succeeded; \
}; \
static inline void FPatchCompleteDelegate_DelegateWrapper(const FMulticastScriptDelegate& PatchCompleteDelegate, bool Succeeded) \
{ \
	_Script_PatcherDemo_eventPatchCompleteDelegate_Parms Parms; \
	Parms.Succeeded=Succeeded ? true : false; \
	PatchCompleteDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_SPARSE_DATA
#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDownloadSingleChunk); \
	DECLARE_FUNCTION(execIsDownloadingSingleChunks); \
	DECLARE_FUNCTION(execIsChunkLoaded); \
	DECLARE_FUNCTION(execGetPatchStatus); \
	DECLARE_FUNCTION(execPatchGame); \
	DECLARE_FUNCTION(execInitPatching);


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDownloadSingleChunk); \
	DECLARE_FUNCTION(execIsDownloadingSingleChunks); \
	DECLARE_FUNCTION(execIsChunkLoaded); \
	DECLARE_FUNCTION(execGetPatchStatus); \
	DECLARE_FUNCTION(execPatchGame); \
	DECLARE_FUNCTION(execInitPatching);


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPDGameInstance(); \
	friend struct Z_Construct_UClass_UPDGameInstance_Statics; \
public: \
	DECLARE_CLASS(UPDGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(UPDGameInstance)


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUPDGameInstance(); \
	friend struct Z_Construct_UClass_UPDGameInstance_Statics; \
public: \
	DECLARE_CLASS(UPDGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(UPDGameInstance)


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPDGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPDGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPDGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPDGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPDGameInstance(UPDGameInstance&&); \
	NO_API UPDGameInstance(const UPDGameInstance&); \
public:


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPDGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPDGameInstance(UPDGameInstance&&); \
	NO_API UPDGameInstance(const UPDGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPDGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPDGameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPDGameInstance)


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PatchVersionURL() { return STRUCT_OFFSET(UPDGameInstance, PatchVersionURL); } \
	FORCEINLINE static uint32 __PPO__ChunkDownloadList() { return STRUCT_OFFSET(UPDGameInstance, ChunkDownloadList); }


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_42_PROLOG
#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_RPC_WRAPPERS \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_INCLASS \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_INCLASS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PDGameInstance_h_45_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PATCHERDEMO_API UClass* StaticClass<class UPDGameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PathcerDemo_Source_PatcherDemo_PDGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
