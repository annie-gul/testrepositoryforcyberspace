-----------------------------------------------------------
-- How to configure IIS
-----------------------------------------------------------

1. Go to Start - Turn Windows features on or off
2. Select Internet Information Services. Wait for the install to complete.
3. Go to Start - Internet Information Services (IIS) Manager
4. Expand COMPUTER - Sites - Default Web Site, and open the MIME Types icon
5. On the Actions column, click Add, and add a MIME type for .pak - application/octet-stream

-----------------------------------------------------------
-- How to do a Cloud Server Deploy
-----------------------------------------------------------

1. Edit /Config/DefaultGame.ini to point to your CDN URL. Example: +CdnBaseUrls=127.0.0.1/PatcherDemo
2. Edit the Patch Version URL on /Loader/LoaderCore/BP_PatcherDemoGameInstance asset to point to your CDN's Build ID provider. Example: 127.0.0.1/PatcherDemo/ContentBuildId.txt

(NOTE: if you're just deploying the pre-packaged build, ignore these)

3. Package your project.
4. Go to your packaged build's /PatcherDemo/Content/Paks/ folder
5. MOVE all the .pak files EXCEPT FOR pakChunk0 to the /CloudStaging/PatcherV0001/Windows/ folder. The ONLY file left on /Content/Paks/ should be pakChunk0.pak
6. Edit the BuildManifest files on /CloudStaging/PatcherV0001/. The second field should exactly match the corresponding .pak file's size in Bytes (from Windows file properties)

(NOTE: continue from here if you're deploying pre-packaged build)

7. Copy the CONTENT of /CloudStaging/ folder to your CDN deployment folder. NOTE: contents only; you shouldn't end up with a CloudStaging/ folder on your CDN
8. Once this is done, you can test manifest retrieval and pak download from editor. Pak mounting won't work. Check Output Log for errors/warnings
9. You should also be able to run the packaged client correctly. Check the Log file if there's errors: search for LogPatch entries
10. If you run into crashes mid-download, try deleting the /Saved/PersistentDownloadDir/ folder to clear the Pak download cache.